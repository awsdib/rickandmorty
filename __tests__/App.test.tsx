import { shallow, render } from 'enzyme';
import React from 'react';
import App from '../App';

const createTestProps = (props: object) => ({
    ...props,
  });

it("App render matches snapshot", () => {
    const wrapper = shallow(
      <App />
    );
    expect(wrapper).toMatchSnapshot();
});

describe('App renders content', () => {
  const props = createTestProps({});
  const wrapper = render(<App {...props} />);

  it('should render a <View />', () => {
    expect(wrapper.find('View')).toHaveLength(1);
  });
});