import { render } from 'react-native-testing-library'
import { SearchList } from '../SearchList'
import { ListItem, SearchBar } from 'react-native-elements';
import React from 'react';

const mockData = [
  {
    id: 'id-1',
    name: 'char-1',
  },
  {
    id: 'id-2',
    name: 'char-2',
  },
  {
    id: 'id-3',
    name: 'char-3',
  },
]

describe('Testing FlatList items', () => {
    test('List component should render', () => {
      const componentTree = render(
        <SearchList />,
      )

      expect(componentTree.getAllByType(SearchList).length).toBe(1)
      expect(componentTree.getAllByType(ListItem).length).toBe(mockData.length)
    })
})