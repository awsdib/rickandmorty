import React, { Component } from 'react';
import { View, Text, FlatList, ActivityIndicator } from 'react-native';
import { ListItem, SearchBar } from 'react-native-elements';

import axiosService from './axiosService';

interface IState {
    loading?: boolean,
    loadingMore?: boolean,
    data?: any,
    page?: number,
    error?: null,
    value?: string
}

export class SearchList extends Component <{}, IState> {
  arrayholder = [];
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      loadingMore: false,
      data: [],
      page: 1,
      error: null,
    };

    this.arrayholder = [];
  }

  componentDidMount() {
    this._fetchAllItems();
  }

  _fetchAllItems = () => {
    const  {page}  = this.state;
    const URL = `/character/?page=${page}`;
    this.setState({ loading: true });
    console.log("b4 ax ")

    axiosService
      .request({
        url: URL,
        method: 'GET'
      })
      .then(response => {
        console.log("response: " + response.data)
        this.setState((prevState, nextProps) => ({
          data:
            page === 1
              ? Array.from(response.data.results)
              : [...this.state.data, ...response.data.results],
          loading: false
        }));
        this.arrayholder = this.state.data;
        
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  
  _LoadMore = () => {
    console.log("more: " + this.state.page)
    this.setState(
      (prevState, nextProps) => ({
        page: prevState.page + 1,
        loadingMore: true
      }),
      () => {
        console.log("after more: " + this.state.page);
        this._fetchAllItems();
      }
    );
  };

  renderHeader = () => {    
    return (      
        <SearchBar
            placeholder={ this.state.value!=""? this.state.value : "Type Here..."}
            lightTheme        
            round        
            onChangeText={text => this.searchFilterFunction(text)}
            autoCorrect={false}             
        />    
    );  
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '86%',
          backgroundColor: '#CED0CE',
          marginLeft: '14%',
        }}
      />
    );
  };

  searchFilterFunction = text => {
    this.setState({
      value: text,
    });

    const newData = this.arrayholder.filter(item => {
      const itemData = `${item.name.toUpperCase()}`;
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      data: newData,
    });
  };

    render() {
        return (
            <FlatList
            data={this.state.data}
            keyExtractor={item => item.id.toString()}
            renderItem={({ item }) => ( 
                <ListItem              
                    roundAvatar              
                    title={`${item.name}`}  
                    subtitle={item.status}                           
                    containerStyle={{ borderBottomWidth: 0 }} 
                    leftAvatar={{ source: { uri: item.image } }}
                />          
            )}
                ItemSeparatorComponent={this.renderSeparator} 
                ListHeaderComponent={this.renderHeader}
                onEndReached={this._LoadMore}
                onEndReachedThreshold={0.5}
            />
        );
    }
}