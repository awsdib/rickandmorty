import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import {SearchList} from './SearchList'

export default function App() {
  return (
    <View style={{ marginTop: 40 }}>
      <SearchList />
    </View>
  );
}